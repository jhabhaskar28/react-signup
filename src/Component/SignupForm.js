import React from "react";
import validator from 'validator';
import '../SignupForm.css';

class SignupForm extends React.Component{
    constructor(){

        super();
    
        this.state = {
          firstName: '',
          lastName: '',
          age: '',
          gender: '',
          role: '',
          email: '',
          password: '',
          repeatPassword: '',
          isCheckedTOC: false,

          formError: {
            firstName: '',
            lastName: '',
            age: '',
            gender: '',
            role: '',
            email: '',
            passwordLengthMessage: '',
            passwordLowercaseMessage: '',
            passwordUppercaseMessage: '',
            passwordNumberMessage: '',
            passwordSymbolMessage: '',
            repeatPassword: '',
            isCheckedTOC: ''
          },
          isErrorInForm: true
        };
    }

    onSubmit = (event) => {

        let errors = {};
        event.preventDefault();

        if(!validator.isAlpha(this.state.firstName)){
          errors.firstName = "Please enter a valid first name";
        }

        if(!validator.isAlpha(this.state.lastName)){
          errors.lastName = "Please enter a valid last name";
        }

        if(!validator.isInt(this.state.age) || this.state.age <= 0 || this.state.age > 200){
          errors.age = "Age should be a positive integer between 1-200";
        }

        if(this.state.gender === ''){
            errors.gender = "Please select your gender";
        }

        if(this.state.role === '' || this.state.role === 'Select your role'){
            errors.role = "Please select your role";
        }

        if(!validator.isEmail(this.state.email)){
          errors.email = "Please enter a valid email address";
        }

        if(!validator.isStrongPassword(this.state.password)){
            if(this.state.password.length <= 7) {
                errors.passwordLengthMessage = "Password length should be minimum 8 characters";
            }
            if(/[a-z]/.test(this.state.password) === false){
                errors.passwordLowercaseMessage = "Password should contain atleast 1 lowercase character";
            }
            if(/[A-Z]/.test(this.state.password) === false){
                errors.passwordUppercaseMessage = "Password should contain atleast 1 uppercase character";
            }
            if(/[0-9]/.test(this.state.password) === false){
                errors.passwordNumberMessage = "Password should contain atleast 1 number";
            }  
            if(/[!@#$%^&*(),.?":{}|<>]/.test(this.state.password) === false){
                errors.passwordSymbolMessage = "Password should contain atleast 1 symbol";
            }       
        }

        if(this.state.repeatPassword !== this.state.password){
          errors.repeatPassword = "Repeat password should not be different from the password";
        }

        if(this.state.isCheckedTOC === false){
          errors.isCheckedTOC = 'Kindly agree to the Terms and Conditions';
        }

        if(Object.keys(errors).length === 0){
            
            this.setState({isErrorInForm: false});

        } else {
            this.setState({
                formError: errors,
                isErrorInForm: true
            });
        }
    }

    render(){
        return (

         this.state.isErrorInForm ?

          <div className="container mainContainer pt-5 mt-5 mb-5">

            <h1 className="text-center text-primary">SIGNUP PAGE</h1>
            
            <div className="container p-5">

              <form className="d-flex flex-column justify-content-center">

                <div className="form-group row">
                  <label htmlFor="exampleFirstName" className="col-sm-2 col-form-label"><i className="fa-solid fa-user"></i> First Name</label>
                  <div className="col-sm-10">
                    <input type="text" className="input-field form-control" id="exampleFirstName" placeholder="Enter First Name" onChange={(event) => {
                      this.setState({firstName: event.target.value});
                    }}
                    value={this.state.firstName}/>
                    <small className="form-text text-danger" >{this.state.formError.firstName}</small>
                  </div>
                </div>     

                <div className="form-group row">
                  <label htmlFor="exampleLastName" className="col-sm-2 col-form-label"><i className="fa-solid fa-user"></i> Last Name</label>
                  <div className="col-sm-10">
                    <input type="text" className="form-control" id="exampleLastName" placeholder="Enter Last Name" onChange={(event) => {
                      this.setState({lastName: event.target.value});
                    }}
                    value={this.state.lastName}/>
                    <small id="lastNameError" className="form-text text-danger">{this.state.formError.lastName}</small>
                  </div>
                </div>

                <div className="form-group row">
                  <label htmlFor="exampleAge" className="col-sm-2 col-form-label"><i className="fa-solid fa-person-cane"></i> Age</label>
                  <div className="col-sm-3">
                    <input type="number" className="form-control" id="exampleAge" placeholder="Enter Age" onChange={(event) => {
                      this.setState({age: event.target.value});
                    }}
                    value={this.state.age}/>
                    <small id="ageError" className="form-text text-danger">{this.state.formError.age}</small>
                  </div>
                  <label className="col-sm-2 col-form-label"></label>
                  <label htmlFor="inlineRadio1" className="col-sm-2 col-form-label"><i className="fa-solid fa-venus-mars"></i> Gender</label>
                  <div className="col-sm-3">
                      <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" 
                        checked = {this.state.gender === "option1"}
                        onChange = {(event) => {
                          this.setState({gender: event.target.value});
                        }}/>
                        <label className="form-check-label" htmlFor="inlineRadio1">Male</label>
                      </div>
                      <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" 
                        checked = {this.state.gender === "option2"}
                        onChange = {(event) => {
                          this.setState({gender: event.target.value});
                        }}/>
                        <label className="form-check-label" htmlFor="inlineRadio2">Female</label>
                      </div>
                      <div className="form-check form-check-inline">
                        <input className="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" 
                        checked = {this.state.gender === "option3"}
                        onChange = {(event) => {
                          this.setState({gender: event.target.value});
                        }}/>
                        <label className="form-check-label" htmlFor="inlineRadio3">Other</label>
                      </div>
                      <small id="ageError" className="form-text text-danger">{this.state.formError.gender}</small>
                  </div>
                </div>

                <div className="form-group row">
                  <label htmlFor="exampleFormControlSelect1" className="col-sm-2 col-form-label"><i className="fa-solid fa-user-graduate"></i> Role</label>
                  <div className="col-sm-10">
                    <select className="form-control" id="exampleFormControlSelect1" value={this.state.role} onChange={(event) => {
                      this.setState({role: event.target.value});
                    }}>
                      <option>Select your role</option>
                      <option>Developer</option>
                      <option>Senior Developer</option>
                      <option>Lead Engineer</option>
                      <option>CTO</option>
                    </select>
                    <small id="roleError" className="form-text text-danger">{this.state.formError.role}</small>
                  </div>
                </div>

                <div className="form-group row">
                  <label htmlFor="exampleEmail" className="col-sm-2 col-form-label"><i className="fa-solid fa-envelope"></i> Email</label>
                  <div className="col-sm-10">
                    <input type="email" className="form-control" id="exampleEmail" placeholder="Enter email" onChange={(event) => {
                      this.setState({email: event.target.value});
                    }}
                    value={this.state.email}/>
                    <small id="emailError" className="form-text text-danger">{this.state.formError.email}</small>
                  </div>
                </div>

                <div className="form-group row">
                  <label htmlFor="exampleInputPassword" className="col-sm-2 col-form-label"><i className="fa-solid fa-key"></i> Password</label>
                  <div className="col-sm-10">
                    <input type="password" className="form-control" id="exampleInputPassword" placeholder="Password" onChange={(event) => {
                      this.setState({password: event.target.value});
                    }}
                    value={this.state.password}/>
                    <small id="passwordError" className="form-text text-danger">{this.state.formError.passwordLengthMessage}</small>
                    <small id="passwordError" className="form-text text-danger">{this.state.formError.passwordLowercaseMessage}</small>
                    <small id="passwordError" className="form-text text-danger">{this.state.formError.passwordUppercaseMessage}</small>
                    <small id="passwordError" className="form-text text-danger">{this.state.formError.passwordNumberMessage}</small> 
                    <small id="passwordError" className="form-text text-danger">{this.state.formError.passwordSymbolMessage}</small>
                  </div>
                </div>

                <div className="form-group row">
                  <label htmlFor="exampleRepeatPassword" className="col-sm-2 col-form-label repeat"><i className="fa-solid fa-key"></i> Repeat Password</label>
                  <div className="col-sm-10">
                    <input type="password" className="form-control" id="exampleRepeatPassword" placeholder="Repeat Password" onChange={(event) => {
                      this.setState({repeatPassword: event.target.value});
                    }}
                    value={this.state.repeatPassword}/>
                    <small id="repeatPasswordError" className="form-text text-danger">{this.state.formError.repeatPassword}</small>
                  </div>
                </div>

                <div className="form-check">
                  <input type="checkbox" className="form-check-input" id="exampleCheck1" checked={this.state.isCheckedTOC}
                    onChange={(event) => {
                      this.setState({isCheckedTOC: event.target.checked});
                    }}/>
                  <div>
                    <label className="form-check-label" htmlFor="exampleCheck1">I agree to the <span id="toc">Terms and Conditions</span></label>
                    <small id="termsError" className="form-text text-danger">{this.state.formError.isCheckedTOC}</small>
                  </div>
                </div>

                <button type="submit" className="btn btn-primary m-5 " onClick={this.onSubmit}>Submit</button>
              </form>
            </div>
          </div>
          :
          <div className="successMessage">
            <h2>Form submitted successfully!</h2>
          </div>
        );
    }

}

export default SignupForm;