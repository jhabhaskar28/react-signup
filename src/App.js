import React from 'react';
import SignupForm from './Component/SignupForm';
import './App.css';

class App extends React.Component {
  render(){
    return(
      <SignupForm />
    );
  }
}

export default App;
